#!/usr/bin/make -f

#export DH_VERBOSE=1

include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/pkg-info.mk

CFLAGS += -Wall -Wextra -Wno-shadow -Werror -g

export DEB_BUILD_MAINT_OPTIONS = hardening=+all future=+all qa=+all reproducible=+all

# make sure TeX/dvipdfm generates reproducable builds
export FORCE_SOURCE_DATE=1

%:
	dh $@ --no-parallel

override_dh_auto_build:
ifeq ($(DEB_HOST_ARCH_OS),linux)
	# Building for a Linux-type OS, everything's fine
	#
	# variables are:
	# Package name      $(DEB_SOURCE)
	# Full version      $(FULLVERSION)
	# Upstream version  $(DEB_VERSION_UPSTREAM)
else
	####################################################
	## Oops, it seems you are trying to build blkutils
	## for a non-Linux OS `$(DEB_HOST_ARCH_OS)'.
	## This is not supported.
	## Please file a bug if you think this is wrong.
	####################################################
	false
endif
	dh_auto_build --no-parallel -- CFLAGS="$(CPPFLAGS) $(CFLAGS) $(LDFLAGS)" all docs

override_dh_auto_install:
	$(MAKE) install \
		prefix=/usr \
		mandir=\$${prefix}/share/man \
		DESTDIR=$(CURDIR)/debian/$(DEB_SOURCE)
	# move admin-only stuff to /usr/sbin
	cd $(CURDIR)/debian/$(DEB_SOURCE) && \
		mv usr/bin/btrace    usr/sbin/ && \
		mv usr/bin/blktrace  usr/sbin/ && \
		mv usr/bin/btreplay  usr/sbin/ && \
		mv usr/bin/btrecord  usr/sbin/
	# fix name of bno_plot
	cd $(CURDIR)/debian/$(DEB_SOURCE) && \
		mv usr/bin/bno_plot.py usr/bin/bno_plot

override_dh_compress:
	dh_compress -X.pdf
