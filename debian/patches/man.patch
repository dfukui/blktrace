Last-Update: 2015-03-25
Forwarded: yes, 2018-05-20
Author: Dmitry Smirnov <onlyjob@member.fsf.org>
Description: man page lintianisation: hyphen-used-as-minus-sign

--- a/doc/iowatcher.1
+++ b/doc/iowatcher.1
@@ -15,13 +15,13 @@
 \fB--help\fP
 Print a brief usage summary.
 .TP
-\fB-d, --device\fP \fIdevice\fP
+\fB-d, \--device\fP \fIdevice\fP
 Controls which device you are tracing.  You can only trace one device at a time for now.  It is sent directly to blktrace, and only needed when you are making a new trace.
 .TP
-\fB-D, --blktrace-destination\fP \fIdestination\fP
+\fB-D, \--blktrace-destination\fP \fIdestination\fP
 Destination for blktrace.
 .TP
-\fB-p, --prog\fP
+\fB-p, \--prog\fP
 Run a program while blktrace is run. The program and its arguments must be
 specified after all other options.  Note that this option previously required
 the program to be given as a single argument but it now tells \fBiowatcher\fP
@@ -32,10 +32,10 @@
 the program to be run. This can be useful if the program name could otherwise
 be mistaken for an option.
 .TP
-\fB-K, --keep-movie-svgs\fP
+\fB-K, \--keep-movie-svgs\fP
 Keep the SVG files generated for movie mode.
 .TP
-\fB-t, --trace\fP \fIpath\fP
+\fB-t, \--trace\fP \fIpath\fP
 Specify the name of the file or directory in which blktrace output is located.
 \fBiowatcher\fP uses a dump from blkparse, so this option tries to guess the
 name of the corresponding per-CPU blktrace data files if the dump file doesn't
@@ -44,10 +44,10 @@
 will use the name of the directory as the base name of the dump file and all
 trace files found inside the directory will be processed.
 .TP
-\fB-l, --label\fP \fIlabel\fP
+\fB-l, \--label\fP \fIlabel\fP
 Sets a label in the graph for a trace file.  The labels are added in the same order as the trace files.
 .TP
-\fB-m, --movie\fP \fI[style]\fP
+\fB-m, \--movie\fP \fI[style]\fP
 Create a movie.  The file format depends on the extension used in the \fB-o\fP
 \fIfile\fP option.  If you specify an .ogv or .ogg extension, the result will
 be Ogg Theora video, if png2theora is available.  If you use an .mp4 extension,
@@ -56,43 +56,43 @@
 are \fBspindle\fP for a circular disc-like effect (default) or \fBrect\fP for a
 rectangular graph style.
 .TP
-\fB-T, --title\fP \fItitle\fP
+\fB-T, \--title\fP \fItitle\fP
 Set a \fItitle\fP to be placed at the top of the graph.
 .TP
-\fB-o, --output\fP \fIfile\fP
+\fB-o, \--output\fP \fIfile\fP
 Output filename for the SVG image or video. The video format used will depend
 on the file name extension. See \fB--movie\fP for details.
 .TP
-\fB-r, --rolling\fP \fIseconds\fP
+\fB-r, \--rolling\fP \fIseconds\fP
 Control the duration for the rolling average.  \fBiowatcher\fP tries to smooth out bumpy graphs by averaging the current second with seconds from the past.  Larger numbers here give you flatter graphs.
 .TP
-\fB-h, --height\fP \fIheight\fP
+\fB-h, \--height\fP \fIheight\fP
 Set the height of each graph
 .TP
-\fB-w, --width\fP \fIwidth\fP
+\fB-w, \--width\fP \fIwidth\fP
 Set the width of each graph
 .TP
-\fB-c, --columns\fP \fIcolumns\fP
+\fB-c, \--columns\fP \fIcolumns\fP
 Number of columns in graph output
 .TP
-\fB-x, --xzoom\fP \fImin:max\fP
+\fB-x, \--xzoom\fP \fImin:max\fP
 Limit processed time range to \fImin:max\fP.
 .TP
-\fB-y, --yzoom\fP \fImin:max\fP
+\fB-y, \--yzoom\fP \fImin:max\fP
 Limit processed sectors to \fImin:max\fP.
 .TP
-\fB-a, --io-plot-action\fP \fIaction\fP
+\fB-a, \--io-plot-action\fP \fIaction\fP
 Plot \fIaction\fP (one of Q, D, or C) in the IO graph.
 .TP
-\fB-P, --per-process-io\fP
+\fB-P, \--per-process-io\fP
 Distinguish between processes in the IO graph.
 .TP
-\fB-O, --only-graph\fP \fIgraph\fP
+\fB-O, \--only-graph\fP \fIgraph\fP
 Add a single graph to the output (see section \fBGRAPHS\fP for options).  By
 default all graphs are included. Use \fB-O\fP to generate only the required
 graphs.  \fB-O\fP may be used more than once.
 .TP
-\fB-N, --no-graph\fP \fItype\fP
+\fB-N, \--no-graph\fP \fItype\fP
 Remove a single graph from the output (see section \fBGRAPHS\fP for options).
 This option may be used more than once.
 .SH GRAPHS
@@ -104,37 +104,37 @@
 Generate graph from the existing trace.dump:
 .PP
 .RS
-# iowatcher -t trace
+# iowatcher \-t trace
 .RE
 .PP
 Skip the IO graph:
 .PP
 .RS
-# iowatcher -t trace.dump -o trace.svg -N io
+# iowatcher \-t trace.dump \-o trace.svg \-N io
 .RE
 .PP
 Only graph tput and latency:
 .PP
 .RS
-# iowatcher -t trace.dump -o trace.svg -O tput -O latency
+# iowatcher \-t trace.dump \-o trace.svg \-O tput \-O latency
 .RE
 .PP
 Generate a graph from two runs, and label them:
 .PP
 .RS
-# iowatcher -t ext4.dump -t xfs.dump -l Ext4 -l XFS -o trace.svg
+# iowatcher \-t ext4.dump \-t xfs.dump \-l Ext4 \-l XFS \-o trace.svg
 .RE
 .PP
 Run a fio benchmark and store the trace in trace.dump, add a title to the top, use /dev/sda for blktrace:
 .PP
 .RS
-# iowatcher -d /dev/sda -t trace.dump -T 'Fio Benchmark' -p fio some_job_file
+# iowatcher \-d /dev/sda \-t trace.dump \-T 'Fio Benchmark' \-p fio some_job_file
 .RE
 .PP
 Make a movie from an existing trace:
 .PP
 .RS
-# iowatcher -t trace --movie -o trace.mp4
+# iowatcher \-t trace \--movie \-o trace.mp4
 .RE
 
 .SH AUTHORS
