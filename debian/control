Source: blktrace
Section: utils
Priority: optional
Maintainer: Bas Zoetekouw <bas@debian.org>
Uploaders: Dmitry Smirnov <onlyjob@debian.org>
Build-Depends:
    debhelper-compat (= 13),
    ghostscript,
    libaio-dev,
    texlive-latex-base,
    texlive-latex-extra
Standards-Version: 4.7.0
Homepage: https://git.kernel.org/pub/scm/linux/kernel/git/axboe/blktrace.git/about/
Vcs-Browser: https://salsa.debian.org/debian/blktrace
Vcs-Git: https://salsa.debian.org/debian/blktrace.git
Rules-Requires-Root: no

Package: blktrace
Architecture: linux-any
Depends: ${misc:Depends}
    ,${shlibs:Depends}
    ,python3
Recommends: libtheora-bin
           ,ffmpeg
           ,librsvg2-bin
Suggests: gnuplot-x11
Description: utilities for block layer IO tracing
 blktrace is a block layer IO tracing mechanism which provides detailed
 information about request queue operations up to user space. There are
 three major components that are provided:
 .
 blktrace: A utility which transfers event traces from the kernel
 into either long-term on-disk storage, or provides direct formatted
 output (via blkparse).
 .
 blkparse: A utility which formats events stored in files, or when
 run in live mode directly outputs data collected by blktrace.
 .
 iowatcher: A utility to visualize block I/O patterns. It generates graphs
 from blktrace runs to help visualize IO patterns and performance. It can
 plot multiple blktrace runs together, making it easy to compare the
 differences between different benchmark runs.
